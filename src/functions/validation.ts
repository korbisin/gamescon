import * as Joi from '@hapi/joi';
import {User} from "../interfaces/user.interface";
import {Result} from "../interfaces/result.interface";
import {ValidationException} from "../helpers/errors/ValidationException";
import {Payload} from "../interfaces/payload.interface";

export const handler = async (event: Payload) => {
    const schema: Joi.ObjectSchema = determineSchema(event);
    console.log(schema);
    const result = schema.validate(event);
    if (result.error) throw new ValidationException(result.error);
    return true;
};

function determineSchema(payload: any): Joi.ObjectSchema {
    if (payload instanceof User) {
        return UserSchema;
    } else if (payload instanceof Result) {
        return ResultSchema;
    }
    throw new Error("Bad input");
}

const UserSchema = Joi.object().keys({
    name: Joi.string().required(),
    surname: Joi.string().required(),
    username: Joi.string().required(),
    password: Joi.string().required()
});

const ResultSchema = Joi.object().keys({
    user_id: Joi.number().required(),
    game_type: Joi.string().required(),
    result: Joi.number().required()
});
