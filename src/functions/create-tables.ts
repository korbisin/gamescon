import {APIGatewayEvent} from "aws-lambda";
import {clientConfig} from "../DB/config";
import {userTable} from "../DB/config";
import {resultTable} from "../DB/config";

const {Client} = require('pg');

export const handler = async (event: APIGatewayEvent): Promise<any> => {

    try {
        const client = new Client(clientConfig);
        await client.connect();
        await client.query(userTable);
        await client.query(resultTable);
    } catch (err) {
        return {
            statusCode: 500,
            body: JSON.stringify(err)
        }
    }

    return {
        statusCode: 200,
        body: JSON.stringify("Tables successfully created")
    }
};