export const clientConfig: object = {
    host: process.env.POSTGRESQL_HOST,
    // @ts-ignore
    port: +process.env.POSTGRESQL_PORT,
    database: process.env.DB_NAME,
    user: process.env.USERNAME,
    password: process.env.PASSWORD
};

export const userTable: string = `CREATE TABLE IF NOT EXISTS users(
                                id serial not null PRIMARY KEY, 
                                name varchar(20) not null,
                                surname varchar(20) not null, 
                                username varchar(20) not null,
                                password varchar(20) not null
                            );`;

export const resultTable: string = `CREATE TABLE IF NOT EXISTS results(
                                id serial not null PRIMARY KEY, 
                                user_id integer REFERENCES users(id),
                                game_type varchar(10) not null, 
                                result numeric(20,2) not null
                            );`;

export let addUser:string = `INSERT INTO users (name, surname, username, password) 
                              VALUES ( $name, $surname, $usernmame, $password);`;

export let addResult:string = `INSERT INTO results (user_id, game_type, result) 
                              VALUES ( $user_id, $game_type, $result);`;