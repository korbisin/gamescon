import {Payload} from "./payload.interface";

export class Result implements Payload {
    user_id: number;
    game_type: string;
    result: number;

    constructor(
        user_id: number,
        game_type: string,
        result: number
    ) {
        this.game_type = game_type;
        this.user_id = user_id;
        this.result = result;
    }
}